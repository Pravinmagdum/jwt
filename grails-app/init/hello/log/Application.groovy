package hello.log

import grails.boot.GrailsApp
import grails.boot.config.GrailsAutoConfiguration

import groovy.transform.CompileStatic
import groovy.util.logging.Slf4j

@Slf4j
@CompileStatic
class Application extends GrailsAutoConfiguration {
    static void main(String[] args) {
        log.debug("this is debug")
        log.info("this is info test")
        GrailsApp.run(Application, args)
    }
}