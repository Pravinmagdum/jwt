package hello.log;

import groovy.util.logging.Slf4j;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;

@Slf4j
@Aspect
class Trace {
    //def log1

    @Pointcut("execution(* my.company.myApp.document.DocumentService.add(..))")
    void createDocument(){}

    @Before("execution(* hello.log.*(..))")
    Boolean ValidateToken(JoinPoint jp){

        //String token = jp.args[0] as String
        //tokn validate
        return false;
    }
}
