package hello.log

import grails.core.GrailsApplication
import grails.plugins.*
import groovy.util.logging.Slf4j
import io.jsonwebtoken.JwtException
import io.jsonwebtoken.Jwts
import io.jsonwebtoken.SignatureAlgorithm
import io.jsonwebtoken.security.Keys

import java.security.Key

@Slf4j
class ApplicationController implements PluginManagerAware {

    GrailsApplication grailsApplication
    GrailsPluginManager pluginManager

    def index() {
        log.debug 'test'
        // We need a signing key, so we'll create one just for this example. Usually
// the key would be read from your application configuration instead.
        Key key = Keys.secretKeyFor(SignatureAlgorithm.HS256);

        String jws = Jwts.builder().setSubject("Joe").signWith(key).compact();
        String jws1 = Jwts.builder().signWith(key)
        //Jwts.builder().
        log.info('generated jws token')
        log.info(jws)

        try {

            Jwts.parserBuilder().setSigningKey(key).build().parseClaimsJws(jws);

            //OK, we can trust this JWT
            log.info('all good')


        } catch (JwtException e) {

            log.info('gotcha!!! hacked')
//don't trust the JWT!
        }
        [grailsApplication: grailsApplication, pluginManager: pluginManager]
    }

    def show(){
        log.info('inside show')

    }
}
